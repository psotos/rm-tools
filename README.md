# README #

This README documents how to use, and update the Rolemaster character sheet.

### What is this repository for? ###

* Rolemaster Blank Character Sheet
* 1.0.1

### How do I get use the sheet for my character? ###

* Editing Class
* Editing Skills
* Updating Stats
* Updating hits
* Adding special backgrounds


### How do I add custom or new classes? ###

* Adding the base class statistics
* Adding class skill costs


### How do I add new races? ###
* Blah

### How do I add new Skills? ###